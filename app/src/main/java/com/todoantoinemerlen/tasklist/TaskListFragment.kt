import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.todoantoinemerlen.R
import com.todoantoinemerlen.network.Api
import com.todoantoinemerlen.network.TasksRepository
import com.todoantoinemerlen.task.TaskActivity
import kotlinx.android.synthetic.main.fragment_task_list.*
import kotlinx.android.synthetic.main.item_task.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class TaskListFragment : Fragment() {

    private val tasksRepository = TasksRepository()
    private val tasks = mutableListOf<Task>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_task_list, container, false)
        return rootView
    }

    private val adapter = TaskListAdapter(tasks)

    private val coroutineScope = MainScope()


    companion object {
        const val ADD_TASK_REQUEST_CODE = 666
        const val EDIT_TASK_REQUEST_CODE = 333
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_view.layoutManager = LinearLayoutManager(activity)
        recycler_view.adapter = adapter

        floatingActionButton.setOnClickListener {
            val intent = Intent(activity, TaskActivity::class.java)
            startActivityForResult(intent, ADD_TASK_REQUEST_CODE)
        }
        adapter.onDeleteClickListener = { task ->
            lifecycleScope.launch {
                tasksRepository.deleteTask(task)
            }
        }

        adapter.onEditClickListener = { task ->
            val intent = Intent(activity, TaskActivity::class.java)
            intent.putExtra("task", task)
            startActivityForResult(intent, EDIT_TASK_REQUEST_CODE)
        }

        tasksRepository.taskList.observe(this, Observer {
            tasks.clear()
            tasks.addAll(it)
            adapter.notifyDataSetChanged()
        })
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == ADD_TASK_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val reply = data!!.getSerializableExtra("task") as Task

                lifecycleScope.launch {
                    tasksRepository.addTask(reply)
                }
            }
        }
        if (requestCode == EDIT_TASK_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val reply = data!!.getSerializableExtra("task") as Task
                lifecycleScope.launch {
                    tasksRepository.updateTask(reply)
                }
            }
        }
    }

    override fun onResume() {

        super.onResume()

        lifecycleScope.launch {
            tasksRepository.refresh()
            val userInfo = Api.userService.getInfo().body()!!
            task_list_info.text = "${userInfo.firstName} ${userInfo.lastName}"
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        coroutineScope.cancel()
    }
}
