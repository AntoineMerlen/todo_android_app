import com.squareup.moshi.Json
import java.io.Serializable

class Task (
    @field:Json(name = "id")
    var id: String,
    @field:Json(name = "title")
    var title: String,
    @field:Json(name = "description")
    var description: String = "Description"): Serializable{
}