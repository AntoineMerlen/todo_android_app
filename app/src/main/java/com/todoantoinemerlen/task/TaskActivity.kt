package com.todoantoinemerlen.task

import Task
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.todoantoinemerlen.R
import kotlinx.android.synthetic.main.activity_task.*
import java.util.*

class TaskActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        val task = intent.getSerializableExtra("task") as? Task
        editTitle.setText(task?.title)
        editDescription.setText(task?.description)

        validateButton.setOnClickListener {
            var newTask = Task(
                id = task?.id ?: UUID.randomUUID().toString(),
                title = editTitle.text.toString(),
                description = editDescription.text.toString()
            )
            intent.putExtra("task", newTask)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}
